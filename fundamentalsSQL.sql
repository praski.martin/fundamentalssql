-- TASK 1
-- Register for the Party (SQL for Beginners #3)

-- You received an invitation to an amazing party. Now you need to write an insert statement to add yourself to the table of participants.
-- participants table schema
-- name (string)
-- age (integer)
-- attending (boolean)
-- NOTES:
-- Since alcohol will be served, you can only attend if you are 21 or older
-- You can't attend if the attending column returns anything but true
-- NOTE: Your solution should use pure SQL. Ruby is used within the test cases just to validate your answer.

INSERT INTO participants(name,age,attending)
VALUES ('Tom',22,true);
SELECT * FROM participants;

TASK 2
-- Adults only

-- In your application, there is a section for adults only.
-- You need to get a list of names and ages of users from the users table, who are 18 years old or older.
-- users table schema
-- name
-- age

SELECT *FROM USERS WHERE age >= 18 ;

-- TASK 3
-- SQL Basics: Simple JOIN with COUNT

-- For this challenge you need to create a simple SELECT statement that will return all columns from the people table,
-- and join to the toys table so that you can return the COUNT of the toys
-- people table schema:
-- id
-- name
-- toys table schema:
-- id
-- name
-- people_id
-- You should return all people fields as well as the toy count as "toy_count".

SELECT people.*, count(people.id) as toy_count
FROM people LEFT JOIN toys
ON people.id = toys.people_id GROUP BY people.id ;

-- TASK 4
--  Select Columns

-- Using only SQL, write a query that returns all rows in the custid, custname, and custstate columns from the customers table.
-- ###Table Description for customers:###
-- Column	Data Type	Size	Sample
-- custid	integer	8	4
-- custname	string	50	Anakin Skywalker
-- custstate	string	50	Tatooine
-- custard	string	50	R2-D2

SELECT customers.custid, customers.custname, customers.custstate FROM customers ;

-- TASK 5
-- Simple WHERE and ORDER BY

-- For this challenge you need to create a simple SELECT statement that will return all columns from the people table
-- WHERE their age is over 50
-- people table schema
-- id
-- name
-- age
-- You should return all people fields where their age is over 50 and order by the age descending

SELECT * FROM people WHERE age>50 ORDER BY age ASC

-- TASK 6 On the Canadian Border

-- You are a border guard sitting on the Canadian border. You were given a list of travelers who have arrived at your gate today.
-- You know that American, Mexican, and Canadian citizens don't need visas, so they can just continue their trips.
-- You don't need to check their passports for visas! You only need to check the passports of citizens of all other countries!
-- Select names, and countries of origin of all the travelers, excluding anyone from Canada, Mexico, or The US.
-- travelers table schema
-- name
-- country
-- NOTE: The United States is written as 'USA' in the table.

SELECT * FROM travelers
WHERE country != 'USA'AND country != 'Canada' AND country != 'Mexico'

-- TASK 7 Easy SQL - Ordering

-- Your task is to sort the information in the provided table 'companies' by number of employees (high to low).
-- Returned table should be in the same format as provided:
-- companies table schema
-- id
-- ceo
-- motto
-- employees

SELECT * FROM companies ORDER BY employees desc

-- TASK 8 SQL Basics: Simple MIN / MAX

-- For this challenge you need to create a simple MIN / MAX statement
-- that will return the Minimum and Maximum ages out of all the people.
-- people table schema
-- id
-- name
-- age
-- select table schema
-- age_min (minimum of ages)
-- age_max (maximum of ages)

SELECT min(age) AS age_min,max(age) AS age_max  FROM people

-- TASK 9 SQL Basics: Mod

-- Given the following table 'decimals':
-- ** decimals table schema **
-- id
-- number1
-- number2
-- Return a table with one column (mod) which is the output of number1 modulus number2.

SELECT MOD(number1,number2) AS mod
FROM decimals

-- TASK 10 LowerCase

-- Given a demographics table in the following format:
-- ** demographics table schema **
-- id
-- name
-- birthday
-- race
-- you need to return the same table where all letters are lowercase in the race column.

SELECT id, name ,birthday ,LOWER(race) AS race
FROM demographics

-- TASK 11 translate

-- You are given a table named repositories, format as below:
-- ** repositories table schema **
-- project
-- commits
-- contributors
-- address
-- The table shows project names of major cryptocurrencies,
--  their numbers of commits and contributors and also a random donation address ( not linked in any way :) ).
-- Your job is to remove all numbers in the address column and replace with '!', then return a table in the following format:
-- ** output table schema **
-- project
-- commits
-- contributors
-- address

SELECT project, commits, contributors, TRANSLATE(address,'0123456789','!!!!!!!!!!') AS address
FROM  repositories

-- TASK 12 Collect Tuition

-- You are working for a local school, and you are responsible for collecting tuition from students.
--  You have a list of all students, some of them have already paid tution and some haven't.' ||
--   ' Write a select statement to get a list of all students who haven't paid their tuition yet.
--   The list should include all the data available about these students.
-- students table schema
-- name (string)
-- age (integer)
-- semester (integer)
-- mentor (string)
-- tuition_received (Boolean)

SELECT * FROM students
WHERE tuition_received = 'false'

-- TASK 13 Rounding Decimals

-- Given the following table 'decimals':
-- ** decimals table schema **
-- id
-- number1
-- number2
-- Return a table with two columns (number1, number2)
--  where the values in number1 have been rounded down and the values in number2 have been rounded up.

SELECT FLOOR(number1) AS number1, CEIL(number2) AS number2
FROM decimals

-- TASK 14 Messi goals function

-- Messi goals function
-- Messi is a soccer player with goals in three leagues:
-- LaLiga
-- Copa del Rey
-- Champions
-- Complete the function to return his total number of goals in all three leagues.
-- Note: the input will always be valid.
-- For example:
-- 5, 10, 2  -->  17
-- you will be given a table, goals, with columns la_liga_goals, copa_del_rey_goals, and champions_league_goals.
-- Return a table with a column, res.

SELECT (la_liga_goals + copa_del_rey_goals + champions_league_goals) AS res FROM goals

-- TASK 15 Raise to the Power

-- Given the following table 'decimals':
-- ** decimals table schema **
-- id
-- number1
-- number2
-- Return a table with one column (result) which is the output of number1 raised to the power of number2.

SELECT POWER(number1,number2) AS result FROM decimals

-- TASK 16  Repeat and Reverse

-- Using our monsters table with the following schema:
-- monsters table schema
-- id
-- name
-- legs
-- arms
-- characteristics
-- return the following table:
-- ** output schema**
-- name
-- characteristics
-- Where the name is the original string repeated three times (do not add any spaces),
-- and the characteristics are the original strings in reverse (e.g. 'abc, def, ghi' becomes 'ihg ,fed ,cba').

SELECT replace(REPEAT(name, 3),' ','') AS name , REVERSE(characteristics) AS characteristics
FROM monsters

-- TASK 17 SELECT WHERE AND OR ORDER BY

-- There is truly no magic in the world; the Hogwarts Sorting Hat is SQL-based, its decision-making powers
-- are common operators and prospectIve students are merely data - names, and two columns of qualities.
-- students
-- id
-- name
-- quality1
-- quality2
-- Slytherin are being quite strict this year and will only take students who are evil AND cunning.
-- Gryffindor will take students who are brave but only if their second quality is NOT evil.
-- Ravenclaw accepts students who are studious OR intelligent.
-- Hufflepuff will simply take those who have the quality hufflepuff.
-- (don't worry, for simplicity's sake 'brave' and 'studious' will only appear in quality1,
-- and 'cunning' and 'intelligent' will only appear in quality2.)
-- Return the id, name, quality1 and quality2 of all the students who'll be accepted, ordered by ascending id.

SELECT * FROM students
WHERE (quality1 = 'evil' AND quality2 = 'cunning')
OR (quality1 = 'brave' AND quality2 != 'evil')
OR(quality1 = 'studious' OR quality2 = 'intelligent')
OR(quality1 = 'hufflepuff' OR quality2 = 'hufflepuff')
ORDER BY id ASC

-- TASK 18 Concatenating Columns

-- Given the table below:
-- ** names table schema **
-- id
-- prefix
-- first
-- last
-- suffix
-- Your task is to use a select statement to return a single column table
-- containing the full title of the person (concatenate all columns together except id), as follows:
-- ** output table schema **
-- title
-- Don't forget to add spaces.

SELECT CONCAT(prefix,' ', first,' ',last,' ',suffix) AS title FROM names

-- TASK 19 GROUP BY

-- For this challenge you need to create a simple GROUP BY statement,
--  you want to group all the people by their age and count the people who have the same age.
-- people table schema
-- id
-- name
-- age
-- select table schema
-- age [group by]
-- people_count (people count)

SELECT age, count(name) as people_count
FROM people
GROUP BY age

-- TASK 20 Rounding Decimals

-- Given the following table 'decimals':
-- ** decimals table schema **
-- id
-- number1
-- number2
-- Return a table with two columns (number1, number2) where the values in number1 have been rounded down
--  and the values in number2 have been rounded up.

SELECT FLOOR(number1) AS number1, CEILING(number2) AS number2  FROM decimals

-- TASK 21 SUM

-- For this challenge you need to create a simple SUM statement that will sum all the ages.
-- people table schema
-- id
-- name
-- age
-- select table schema
-- age_sum (sum of ages)

SELECT SUM(age) AS age_sum
FROM people

-- TASK 22 Up and Down

-- Given a table of random numbers as follows:
-- ** numbers table schema **
-- id
-- number1
-- number2
-- Your job is to return table with similar structure and headings, where if the sum of a column is odd,
--  the column shows the minimum value for that column, and when the sum is even, it shows the max value.
--  You must use a case statement.
-- ** output table schema **
-- number1
-- number2

SELECT
  CASE WHEN MOD(SUM(number1), 2) = 1 THEN MIN(number1)
    ELSE MAX(number1)
  END AS number1,
  CASE WHEN MOD(SUM(number2), 2) = 0 THEN MAX(number2)
    ELSE MIN(number2)
  END AS number2
FROM numbers

-- TASK 23 Length based SELECT with LIKE

-- You will need to create SELECT statement in conjunction with LIKE.
-- Please list people which have first_name with at least 6 character long
-- names table schema
-- id
-- first_name
-- last_name
-- results table schema
-- first_name
-- last_name

SELECT first_name, last_name
FROM names
WHERE first_name
LIKE '______%'

-- TASK 24 Bit Length

-- Given a demographics table in the following format:
-- ** demographics table schema **
-- id
-- name
-- birthday
-- race
-- you need to return the same table where all text fields (name & race) are changed to the bit length of the string.

SELECT
  id,
  BIT_LENGTH(name) AS name,
  birthday,
  BIT_LENGTH(race) AS race
FROM demographics

-- TASK 25 WHERE, LIKE, CONCAT, INITCAP

-- Deep within the fair realm of Lothlórien,
-- you have been asked to create a shortlist of candidates for a recently vacated position on the council.
-- Of so many worthy elves, who to choose for such a lofty position?
-- After much thought you decide to select candidates by name,
-- which are often closely aligned to an elf's skill and temperament.
-- Choose those with tegil appearing anywhere in their first name, as they are likely to be good calligraphers,
-- OR those with astar anywhere in their last name, who will be faithful to the role.
-- Elves table:
-- firstname
-- lastname
-- all names are in lowercase
-- To aid the scribes, return the firstname and lastname column concatenated, separated by a space,
-- into a single shortlist column, and capitalise the first letter of each name.

SELECT CONCAT(INITCAP(firstname),' ',INITCAP(lastname)) AS shortlist
FROM Elves
WHERE firstname LIKE '%tegil%' OR lastname LIKE '%astar%'

-- TASK 26 Opposite number

-- Very simple, given a number, find its opposite.
-- Examples:
-- 1: -1
-- 14: -14
-- -34: 34
-- You will be given a table: opposite, with a column: number. Return a table with a column: res.

SELECT number * -1 AS res
FROM opposite

-- TASK 27 COUNT, GROUP BY, ORDER BY, ASC, DESC

-- You are the owner of the Grocery Store.
-- All your products are in the database, that you have created after CodeWars SQL excercises!:)
-- You have reached a conclusion that you waste to much time
-- because you have to many different warehouse to visit each week.
-- You have to find out how many unique products have each of the Producer.
-- If you take only few items from some of them you will stop going there to save the gasoline:)
-- In the results show producer and unique_products which you buy from him.
-- Order the result by unique_products (DESC) then by producer (ASC) in case there are duplicate amounts,
-- products table schema
-- id
-- name
-- price
-- stock
-- producer
-- results table schema
-- unique_products
-- producer

SELECT COUNT(name) AS unique_products,producer
FROM products
GROUP BY producer
ORDER BY unique_products DESC, producer ASC

-- TASK 28 ORDER BY, DESC

-- You work at a book store.
-- It's the end of the month, and you need to find out the 5 bestselling books at your store.
-- Use a select statement to list names, authors, and number of copies sold of the 5 books which were sold most.
-- books table schema
-- name
-- author
-- copies_sold

SELECT * FROM books ORDER BY copies_sold DESC LIMIT 5

-- TASK 29 SELECT

-- Terminal game move function
-- In this game, the hero moves from left to right.
-- The player rolls the dice and moves the number of spaces indicated by the dice two times.
-- In SQL, you will be given a table moves with columns position and roll.
-- Return a table which uses the current position of the hero and the roll (1-6)
-- and returns the new position in a column res.
-- Example
-- move(3, 6) should equal 15

SELECT position + roll*2 AS res
FROM moves

-- TASK 30 GROUP BY

-- You are the owner of the Grocery Store.
-- All your products are in the database.
-- You care about local market, and want to check how many products come from United States of America or Canada.
-- Please use SELECT statement and IN to filter out other origins.
-- In the results show how many products are from United States of America and Canada respectively.
-- Order by number of products, descending.
-- products table schema
-- id
-- name
-- price
-- stock
-- producer
-- country
-- results table schema
-- products
-- country

SELECT count(id) AS products, country
FROM products
WHERE country IN('United States of America','Canada')
GROUP BY country
ORDER BY products DESC

-- TASK 31 modulus

-- Given a Divisor and a Bound , Find the largest integer N , Such That ,
-- Conditions :
-- N is divisible by divisor
-- N is less than or equal to bound
-- N is greater than 0.
-- Notes
-- The parameters (divisor, bound) passed to the function are only positve values .
-- It's guaranteed that a divisor is Found .
-- Input >> Output Examples
-- maxMultiple (2,7) ==> return (6)
-- Explanation:
-- (6) is divisible by (2) , (6) is less than or equal to bound (7) , and (6) is > 0 .
-- maxMultiple (10,50)  ==> return (50)
-- Explanation:
-- (50) is divisible by (10) , (50) is less than or equal to bound (50) , and (50) is > 0 .*
-- maxMultiple (37,200) ==> return (185)
-- Explanation:
-- (185) is divisible by (37) , (185) is less than or equal to bound (200) , and (185) is > 0 .

SELECT bound - bound % divisor AS res
FROM max_multiple

-- TASK 32 CASE

-- You will be given a table, numbers, with one column number.
-- Return a table with a column is_even containing "Even" or "Odd" depending on number column values.
-- numbers table schema
-- number INT
-- output table schema
-- is_even STRING

SELECT
  CASE
    WHEN number % 2 = 0 THEN 'Even'
    ELSE 'Odd'
  END
AS is_even
FROM numbers;

-- TASK 33 WHERE, LIKE, ORDER BY, LIMIT

-- Schema of the countries table:
-- country (String)
-- capital (String)
-- continent (String)
-- The first question: from the African countries that start with the character E,
-- get the names of their capitals ordered alphabetically.
-- You should only give the names of the capitals. Any additional information is just noise
-- If you get more than 3, you will be kicked out, for being too smart
-- Also, this database is crowd-sourced, so sometimes Africa is written Africa and in other times Afrika.

SELECT capital
FROM countries
WHERE (continent = 'Africa' OR continent = 'Afrika') AND country LIKE 'E%'
ORDER BY capital
ASC LIMIT 3

-- TASK 34 SPLIT_PART

-- You have access to a table of monsters as follows:
-- monsters schema
-- id
-- name
-- legs
-- arms
-- characteristics
-- The monsters in the provided table have too many characteristics, they really only need one each.
-- Your job is to trim the characteristics down so that each monster only has one.
-- If there is only one already, provide that.
-- If there are multiple, provide only the first one (don't leave any commas in there).
-- You must return a table with the format as follows:
-- output schema
-- id
-- name
-- characteristic
-- Order by id

SELECT id, name, SPLIT_PART(characteristics,',',1) AS characteristic
FROM monsters
ORDER BY id

-- TASK 35 WHERE

-- Use SELECT to show id, name, stock from
-- products which are only 2 or less item in the stock and are from CompanyA.
-- Order the results by product id
-- products table schema
-- id
-- name
-- price
-- stock
-- producent
-- results table schema
-- id
-- name
-- stock

SELECT id, name, stock
FROM products
WHERE producent = 'CompanyA' AND stock <= 2
ORDER BY id

-- TASK 36 JOIN

-- For this challenge you need to create a simple SELECT statement that will return all columns from
-- the products table, and join to the companies table so that you can return the company name.
-- products table schema
-- id
-- name
-- isbn
-- company_id
-- price
-- companies table schema
-- id
-- name
-- You should return all product fields as well as the company name as "company_name".

SELECT products.*, companies.name AS company_name
FROM products JOIN companies ON company_id = companies.id

-- TASK 37 MAX, AS

-- Given a table of random numbers as follows:
-- -- ** numbers table schema **
-- -- id
-- -- number1
-- -- number2
-- -- number3
-- -- number4
-- -- number5
-- -- Your job is to return a single row table in the following format, where each value is the largest
-- -- value from its corresponding column:
-- -- ** output table schema **
-- -- maxnum1
-- -- maxnum2
-- -- maxnum3
-- -- maxnum4
-- -- maxnum5

SELECT
  MAX(number1) AS maxnum1,
  MAX(number2) AS maxnum2,
  MAX(number3) AS maxnum3,
  MAX(number4) AS maxnum4,
  MAX(number5) AS maxnum5
FROM numbers

-- TASK 38 BETWEEN, ORDER BY

-- You are given a table of persons which includes their respective age.
-- Your task is to return the list of people between ages 30 through 50.
-- The schema for the persons table is as follows:
-- id
-- name
-- age
-- The resulting table should have two columns as follows:
-- name
-- age
-- Order by age, highest to lowest.

SELECT name, age
FROM persons
WHERE age BETWEEN 30 AND 50
ORDER BY age DESC

-- TASK 39 FLOOR

-- Nathan loves cycling.
-- Because Nathan knows it is important to stay hydrated, he drinks 0.5 litres of water per hour of cycling.
-- You get given the time in hours and you need to return the number of litres Nathan will drink, rounded to the smallest value.
-- For example:
-- time = 3 ----> litres = 1
-- time = 6.7---> litres = 3
-- time = 11.8--> litres = 5
-- You have to return 3 columns: id, hours and liters

SELECT *, FLOOR(hours * 0.5) AS liters
FROM cycling

-- TASK 40 DISTINCT

-- For this challenge you need to create a simple DISTINCT statement, you want to find all the unique ages.
-- people table schema
-- id
-- name
-- age
-- select table schema
-- age (distinct)

SELECT DISTINCT age
FROM people

-- TASK 41 COUNT, HAVING

-- For this challenge you need to create a simple HAVING statement, you want to count how many people have the same age
-- and return the groups with 10 or more people who have that age.
-- people table schema
-- id
-- name
-- age
-- return table schema
-- age
-- total_people

SELECT age, COUNT(*) AS total_people
FROM people
GROUP BY age
HAVING COUNT(*) >= 10

-- TASK 42

-- Clock shows 'h' hours, 'm' minutes and 's' seconds after midnight.
-- Your task is to make 'Past' function which returns time converted to milliseconds.
-- Example:
-- {h: 0, m: 1, s: 1} => res: 61000
-- Input constraints: 0 <= h <= 23, 0 <= m <= 59, 0 <= s <= 59

SELECT h * 3600000 + m * 60000 + s * 1000 AS res FROM past

-- TASK 43 TRUNC

-- Given the following table 'decimals':
-- ** decimals table schema **
-- id
-- number1
-- number2
-- Return a table with one column (towardzero) where the values are the result of number1 + number2 truncated towards zero.

SELECT TRUNC(number1 + number2) AS towardzero
FROM decimals

-- TASK 44 INNER JOIN

-- It's time to assess which of the world's greatest fighters are through to the 6 coveted places
-- in the semi-finals of the Street Fighter World Fighting Championship.
-- Every fight of the year has been recorded and each fighter's wins and losses need to be added up.
--
-- Each row of the table fighters records, alongside the fighter's name,
-- whether they won (1) or lost (0), as well as the type of move that ended the bout.
--
-- id
-- name
-- won
-- lost
-- move_id
--
-- winning_moves:
-- id
-- move
--
-- However, due to new health and safety regulations, all ki blasts have been outlawed as a potential fire hazard.
-- Any bout that ended with Hadoken, Shouoken or Kikoken should not be counted in the total wins and losses.
-- So, your job:
-- Return name, won, and lost columns displaying the name, total number of wins and total number of losses.
-- Group by the fighter's name.
-- Do not count any wins or losses where the winning move was Hadoken, Shouoken or Kikoken.
-- Order from most-wins to least
-- Return the top 6. Don't worry about ties.

SELECT name, SUM(won) AS won, SUM(lost) AS lost
FROM fighters
INNER JOIN winning_moves ON fighters.move_id = winning_moves.id
WHERE NOT move IN ('Hadoken', 'Shouoken', 'Kikoken')
GROUP BY name
ORDER BY won DESC LIMIT 6

-- TASK 45 Add index column to a query

-- SQLite
-- Return a new table from people table.
-- Your solution should have a row index number with the column name row number, id and the name of every people alphabetically.
-- This code can be used to normalize in REAL enviroments when you don't have an id column and should generate it from scratch.

SELECT (
  SELECT count(*)
  FROM people AS t2
  WHERE t1.name >= t2.name
  ) AS 'row number',
  id,
  name
FROM people AS t1
ORDER BY name

-- TASK 46 GROUP BY, ORDER BY

-- Given a demographics table in the following format:
-- ** demographics table schema **
-- id
-- name
-- birthday
-- race
-- you need to return a table that shows a count of each race represented, ordered by the count in descending order as:
-- ** output table schema **
-- race
-- count

SELECT race, count(race) AS count
FROM demographics
GROUP BY race
ORDER BY count DESC

-- TASK 47 SELECT RIGHT  and LEFT

-- You are given a table named repositories, format as below:
-- ** repositories table schema **
-- project
-- commits
-- contributors
-- address
-- The table shows project names of major cryptocurrencies,
-- their numbers of commits and contributors and also a random donation address ( not linked in any way :) ).
-- For each row: Return first x characters of the project name where x = commits.
-- Return last y characters of each address where y = contributors.
-- Return project and address columns only, as follows:
-- ** output table schema **
-- project
-- address
-- Case should be maintained.

SELECT LEFT(project, commits) AS project, RIGHT(address, contributors) AS address
FROM repositories

-- TASK 48 bit_length, char_length

-- Given a demographics table in the following format:
-- ** demographics table schema **
-- id
-- name
-- birthday
-- race
-- return a single column named 'calculation' where
-- the value is the bit length of name, added to the number of characters in race.

SELECT bit_length(name)+char_length(race) AS calculation
FROM demographics

-- TASK 49 First and last IP in a network

-- Given a table where users' connections are logged, find the first and the last address of the networks they connected from.
-- Notes
-- Order the result by the id column
-- There's no need to validate anything - it's okay if the user connects from a private network
-- (You don't need the connection_time field for this task but without it the input data looks too dull)
-- You can read more about IPv4 on Wikipedia (check the First and last subnet addresses section
-- if you need an example/explanation related to this task only)
-- Input table
-- ---------------------------------------------
-- |    Table    |     Column      |   Type    |
-- |-------------+-----------------+-----------|
-- | connections | id              | int       |
-- |             | connection_time | timestamp |
-- |             | ip_address      | inet      |
-- ---------------------------------------------
-- Output table
-- ------------------------
-- |    Column     | Type |
-- |---------------+------|
-- | id            | int  |
-- | first_address | text |
-- | last_address  | text |
-- ------------------------
-- Example
-- For the IP address 182.240.42.115/24 the first address in the network is 182.240.42.0/24,
-- and the last one is 182.240.42.255/24.

SELECT id, network(ip_address) AS first_address, broadcast(ip_address) AS last_address
FROM connections
ORDER BY id

-- TASK 50 Grasshopper - Check for factor

-- This function should test if the factor is a factor of base.
-- Return true if it is a factor or false if it is not.
-- About factors
-- Factors are numbers you can multiply together to get another number.
-- 2 and 3 are factors of 6 because: 2 * 3 = 6
-- You can find a factor by dividing numbers. If the remainder is 0 then the number is a factor.
-- You can use the mod operator (%) in most languages to check for a remainder
-- For example 2 is not a factor of 7 because: 7 % 2 = 1
-- Note: base is a non-negative number, factor is a positive number.

SELECT id, base % factor = 0 AS res
FROM kata

-- TASK 51 EXISTS

-- For this challenge you need to create a SELECT statement that will contain data about
-- departments that had a sale with a price over 98.00 dollars.
-- This SELECT statement will have to use an EXISTS to achieve the task.
-- departments table schema
-- id
-- name
-- sales table schema
-- id
-- department_id (department foreign key)
-- name
-- price
-- card_name
-- card_number
-- transaction_date
-- resultant table schema
-- id
-- name

SELECT id ,name
FROM departments AS d
WHERE EXISTS (
  SELECT *
  FROM sales AS s
  WHERE d.id = s.department_id AND s.price>98
)

-- TASK 52 POSITION

-- You have access to a table of monsters as follows:
-- monsters schema
-- id
-- name
-- legs
-- arms
-- characteristics
-- In each row, the characteristic column has a single comma.
-- Your job is to find it using position(). You must return a table with the format as follows:
-- output schema
-- id
-- name
-- comma
-- The comma column will contain the position of the comma within the characteristics string. Order the results by comma.

SELECT id, name, POSITION(',' IN characteristics) AS comma FROM
monsters ORDER BY comma

-- TASK 53 Century From Year

-- Introduction
-- The first century spans from the year 1 up to and including the year 100,
-- The second - from the year 101 up to and including the year 200, etc.
-- Task :
-- Given a year, return the century it is in.
-- Input , Output Examples ::
-- centuryFromYear(1705)  returns (18)
-- centuryFromYear(1900)  returns (19)
-- centuryFromYear(1601)  returns (17)
-- centuryFromYear(2000)  returns (20)
-- In SQL, you will be given a table years with a column yr for the year. Return a table with a column century.

SELECT (yr+99)/100 AS century
FROM years